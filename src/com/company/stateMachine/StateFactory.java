package com.company.stateMachine;

import com.company.enums.StateEnum;

public class StateFactory {
    public static State getState(StateEnum stateType){
        switch (stateType){
            case START:
                return new StartState();
            case MENU_MANAGEMENT:
                return new MenuManagementState();
            case SHOW_MENU:
                return new ShowMenuState();
            case ADD_ITEM:
                return new AddItemState();
            case REMOVE_ITEM:
                return new RemoveItemState();
            case UPDATE_ITEM:
                return new UpdateItemState();
            case ORDER_MANAGEMENT:
                return new OrderManagementState();
            case SHOW_ORDER:
                return new ShowOrderState();
            case CREATE_ORDER:
                return new CreateOrderState();
            case UPDATE_ORDER:
                return new UpdateOrderState();
            case DELETE_ORDER:
                return new DeleteOrderState();
            case ADD_ITEM_TO_ORDER:
                return new AddItemtoOrderState();
            case REMOVE_ITEM_FROM_ORDER:
                return new RemoveItemFromOrderState();
            case PAY_BILL:
                return new PayBillState();
            case BILL_STORAGE_MANAGEMENT:
                return new BillStorageManagementState();
            case SHOW_BILL:
                return new ShowBillState();
            case DELETE_BILL:
                return new DeleteBillState();
            case SEARCH_BILL:
                return new SearchBillState();
            default:
                return null;
        }
    }
}
