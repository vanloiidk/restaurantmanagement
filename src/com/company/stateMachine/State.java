package com.company.stateMachine;

import com.company.enums.StateEnum;

public interface State {
    StateEnum handleRequest();
}
