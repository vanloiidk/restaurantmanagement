package com.company.stateMachine;

import com.company.controller.OrderController;
import com.company.enums.StateEnum;

public class ShowOrderState implements State {

    private final OrderController orderController;

    public ShowOrderState(){
        orderController = new OrderController();
    }
    @Override
    public StateEnum handleRequest() {
        try{
            orderController.showOrder(false);
        }catch (Exception e){
            e.printStackTrace();
            System.err.println("Something went wrong, please try again!");
        }
        return StateEnum.ORDER_MANAGEMENT;
    }
}
