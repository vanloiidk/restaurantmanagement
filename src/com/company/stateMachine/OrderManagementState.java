package com.company.stateMachine;

import com.company.enums.StateEnum;
import com.company.helper.OptionArrayListHelper;
import com.company.model.SelectOption;
import com.company.view.OptionDashBoardBeauty;

import java.util.ArrayList;
import java.util.Scanner;

public class OrderManagementState implements State {
    private final ArrayList<SelectOption> OPTIONS = new ArrayList<SelectOption>(){
        {
            add(new SelectOption(1,"Create new order", StateEnum.CREATE_ORDER));
            add(new SelectOption(2,"Show Order",StateEnum.SHOW_ORDER));
            add(new SelectOption(3,"Pay Bill", StateEnum.PAY_BILL));
            add(new SelectOption(4,"Update Order",StateEnum.UPDATE_ORDER));
            add(new SelectOption(5,"Delete Order", StateEnum.DELETE_ORDER));
            add(new SelectOption(6,"Add item to order",StateEnum.ADD_ITEM_TO_ORDER));
            add(new SelectOption(7,"Remove item from order",StateEnum.REMOVE_ITEM_FROM_ORDER));
            add(new SelectOption(0, "Exist", StateEnum.START));
        }
    };
    private OptionDashBoardBeauty optionDashBoardBeauty;
    public OrderManagementState() {
        optionDashBoardBeauty = new OptionDashBoardBeauty.Builder().hasOptions(OPTIONS).build();
    }

    @Override
    public StateEnum handleRequest() {
        Scanner sc = new Scanner(System.in);
        while(true){
            try{
                optionDashBoardBeauty.printBeauty();
                int option = sc.nextInt();
                return OptionArrayListHelper.getStateName(OPTIONS,option);
            }
            catch (Exception e){
                sc.nextLine();
                System.err.println("Something went Wrong, please try again");
            }

        }    }
}
