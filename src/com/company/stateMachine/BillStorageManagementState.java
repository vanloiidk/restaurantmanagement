package com.company.stateMachine;

import com.company.controller.OrderController;
import com.company.enums.StateEnum;
import com.company.helper.OptionArrayListHelper;
import com.company.model.SelectOption;
import com.company.view.OptionDashBoardBeauty;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class BillStorageManagementState implements State {
    private final ArrayList<SelectOption> OPTIONS = new ArrayList<>(){
        {
            add(new SelectOption(1,"Show Bills",StateEnum.SHOW_BILL));
            add(new SelectOption(2, "Delete Bill",StateEnum.DELETE_BILL));
            add(new SelectOption(3,"Search Bills", StateEnum.SEARCH_BILL));
            add(new SelectOption(0, "Exist", StateEnum.START));
        }
    };
    private final OptionDashBoardBeauty optionDashBoardBeauty = new OptionDashBoardBeauty("Bill Storage Management",OPTIONS);
    @Override
    public StateEnum handleRequest() {
        Scanner sc = new Scanner(System.in);
        while (true){
            try{
                optionDashBoardBeauty.printBeauty();
                int selected = sc.nextInt();
                sc.nextLine();
                return OptionArrayListHelper.getStateName(OPTIONS,selected);

            }catch (InputMismatchException e){
                sc.nextLine();
                System.err.println("Input is invalid");
            }
            catch (Exception e){
                System.err.println("Something went wrong, please try again!");
            }
        }
    }
}
