package com.company.stateMachine;

import com.company.controller.OrderController;
import com.company.enums.StateEnum;

public class CreateOrderState implements State {
    private final OrderController orderController;

    public CreateOrderState(){
        orderController = new OrderController();
    }
    @Override
    public StateEnum handleRequest() {
        try{
            orderController.createOrder();
        }catch (Exception e){
            e.printStackTrace();
            System.err.println("Something went wrong, please try again!");
            System.err.println("Message: "+e.getMessage());
        }
        return StateEnum.ORDER_MANAGEMENT;
    }
}
