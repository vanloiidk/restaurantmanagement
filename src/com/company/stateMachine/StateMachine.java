package com.company.stateMachine;

import com.company.enums.StateEnum;

import java.util.Objects;

public class StateMachine {

    private final static Context context = new Context(new StartState());
    public static void run(){
        while (true){
            StateEnum stateType = context.applyState();
            if(Objects.isNull(stateType)) break;
            State nextState = StateFactory.getState(stateType);
            context.setState(nextState);
        }
    }
}
