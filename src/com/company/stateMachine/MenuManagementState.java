package com.company.stateMachine;

import com.company.enums.StateEnum;
import com.company.helper.OptionArrayListHelper;
import com.company.model.SelectOption;
import com.company.view.OptionDashBoardBeauty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class MenuManagementState implements State {

    private final ArrayList<SelectOption> OPTIONS = new ArrayList<SelectOption>(){
        {
            add(new SelectOption(1,"Show Menu", StateEnum.SHOW_MENU));
            add(new SelectOption(2,"Add item",StateEnum.ADD_ITEM));
            add(new SelectOption(3,"Remove Item", StateEnum.REMOVE_ITEM));
            add(new SelectOption(4,"Update Item",StateEnum.UPDATE_ITEM));
            add(new SelectOption(0, "Exist", StateEnum.START));
        }
    };
    private OptionDashBoardBeauty optionDashBoardBeauty;
    public MenuManagementState() {
        optionDashBoardBeauty = new OptionDashBoardBeauty.Builder().hasOptions(OPTIONS).build();
    }

    @Override
    public StateEnum handleRequest(){
        Scanner sc = new Scanner(System.in);
        while(true){
            try{
                optionDashBoardBeauty.printBeauty();
                int option = sc.nextInt();
                return OptionArrayListHelper.getStateName(OPTIONS,option);
            }
            catch (Exception e){
                sc.nextLine();
                System.err.println("Something went Wrong, please try again");
            }

        }
    }

}
