package com.company.stateMachine;

import com.company.controller.OrderController;
import com.company.enums.StateEnum;

public class ShowBillState implements State {
    private final OrderController orderController = new OrderController();
    @Override
    public StateEnum handleRequest() {
        try{
            orderController.showOrder(true);
        }catch (Exception e){
            System.err.println("Something went wrong, please try again!!");
        }
        return StateEnum.BILL_STORAGE_MANAGEMENT;
    }
}
