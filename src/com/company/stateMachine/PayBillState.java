package com.company.stateMachine;

import com.company.controller.OrderController;
import com.company.enums.StateEnum;

public class PayBillState implements State {
    private final OrderController orderController;

    public PayBillState(){
        orderController = new OrderController();
    }
    @Override
    public StateEnum handleRequest() {
        try{
            orderController.payBill();
        }catch (Exception e){
            System.err.println("Something went wrong, please try again");
        }

        return StateEnum.ORDER_MANAGEMENT;
    }
}
