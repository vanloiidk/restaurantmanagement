package com.company.stateMachine;

import com.company.enums.StateEnum;

public class Context {
    private State state;

    public Context(State state) {
        this.state = state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public StateEnum applyState() {
         return this.state.handleRequest();
    }

    public State getState() {
        return this.state;
    }
}
