package com.company.stateMachine;

import com.company.controller.MenuItemController;
import com.company.controller.OrderController;
import com.company.enums.StateEnum;

public class UpdateOrderState implements State {
    private final OrderController orderController = new OrderController();

    @Override
    public StateEnum handleRequest() {
        try{
            orderController.updateOrder();
        }catch (Exception e){
            System.err.println("Something went wrong, please try again");
            System.err.println("Message: "+e.getMessage());
        }
        return StateEnum.ORDER_MANAGEMENT;
    }
}
