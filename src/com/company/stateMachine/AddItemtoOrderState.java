package com.company.stateMachine;

import com.company.controller.OrderController;
import com.company.enums.StateEnum;

public class AddItemtoOrderState implements State {
    private final OrderController orderController = new OrderController();
    @Override
    public StateEnum handleRequest() {
        try{
            orderController.addItemToOrder();
        }catch (Exception e){
            System.out.println("Something went wrong, please try again");
            System.out.println("Message: "+e.getMessage());
        }

        return StateEnum.ORDER_MANAGEMENT;
    }
}
