package com.company.stateMachine;

import com.company.enums.StateEnum;
import com.company.helper.OptionArrayListHelper;
import com.company.model.SelectOption;
import com.company.view.OptionDashBoardBeauty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class StartState implements State{

    private static final ArrayList<SelectOption> OPTIONS = new ArrayList<SelectOption>(){
        {
            add(new SelectOption(1,"Menu Management", StateEnum.MENU_MANAGEMENT));
            add(new SelectOption(2, "Bills Storage Management", StateEnum.BILL_STORAGE_MANAGEMENT));
            add(new SelectOption(3,"Order Management", StateEnum.ORDER_MANAGEMENT));
            add(new SelectOption(4,"Exist",null));
        }
    };
    private OptionDashBoardBeauty optionDashBoardBeauty;
    public StartState() {
        optionDashBoardBeauty = new OptionDashBoardBeauty.Builder().hasOptions(OPTIONS).build();
    }

    @Override
    public StateEnum handleRequest() {
        Scanner sc = new Scanner(System.in);
        while(true){
            try{
                optionDashBoardBeauty.printBeauty();
                int option = sc.nextInt();
                sc.nextLine();
                return OptionArrayListHelper.getStateName(OPTIONS,option);
            }
            catch (IllegalArgumentException e){
                System.err.println(e.getMessage());
            }catch (Exception e){
                sc.nextLine();
                System.err.println("Something went wrong. please try again");
            }

        }
    }
}
