package com.company.stateMachine;

import com.company.controller.MenuItemController;
import com.company.enums.StateEnum;

public class AddItemState implements State {

    private MenuItemController menuItemController;

    public AddItemState() {
        menuItemController = new MenuItemController();
    }

    @Override
    public StateEnum handleRequest() {
        while (true){
            try{
                menuItemController.createMenuItem();
                System.out.println("Insert item successfully!");
                return StateEnum.MENU_MANAGEMENT;

            }catch (Exception e){
                System.err.println("Something went wrong. please try again");
                return StateEnum.MENU_MANAGEMENT;
            }

        }
    }
}
