package com.company.stateMachine;

import com.company.controller.MenuItemController;
import com.company.enums.StateEnum;

public class ShowMenuState implements State{
    private final MenuItemController menuItemController;

    public ShowMenuState() {
        menuItemController = new MenuItemController();
    }

    @Override
    public StateEnum handleRequest() {
        menuItemController.ShowMenuGroup();
        return StateEnum.MENU_MANAGEMENT;
    }
}
