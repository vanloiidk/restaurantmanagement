package com.company.stateMachine;

import com.company.controller.MenuItemController;
import com.company.enums.StateEnum;

public class RemoveItemState implements State {
    private MenuItemController menuItemController;

    public RemoveItemState() {
        this.menuItemController = new MenuItemController();
    }

    @Override
    public StateEnum handleRequest() {
            try{
                menuItemController.ShowMenu();
                menuItemController.deleteById();
            }catch (Exception e){
                System.err.println(e);
            }
            return StateEnum.MENU_MANAGEMENT;
    }
}
