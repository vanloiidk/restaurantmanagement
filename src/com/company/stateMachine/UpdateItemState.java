package com.company.stateMachine;

import com.company.controller.MenuItemController;
import com.company.enums.StateEnum;

public class UpdateItemState implements State {
    private final MenuItemController menuItemController = new MenuItemController();
    @Override
    public StateEnum handleRequest() {
        try{
            menuItemController.updateItem();
        }catch (Exception e){
            System.err.println("Something went wrong, please try again");
            System.err.println("Message: "+e.getMessage());
        }
        return StateEnum.MENU_MANAGEMENT;
    }
}
