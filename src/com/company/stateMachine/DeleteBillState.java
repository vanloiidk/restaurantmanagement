package com.company.stateMachine;

import com.company.controller.OrderController;
import com.company.enums.StateEnum;

public class DeleteBillState implements State {
    private final OrderController orderController = new OrderController();
    @Override
    public StateEnum handleRequest() {
        try{
            orderController.deleteBill();
        }catch (Exception e){
            System.err.println("Something went wrong, please try again");
            System.err.println("Message: "+e.getMessage());
        }

        return StateEnum.BILL_STORAGE_MANAGEMENT;
    }
}
