package com.company.stateMachine;

import com.company.controller.OrderController;
import com.company.enums.StateEnum;

public class RemoveItemFromOrderState implements State {
    private OrderController orderController = new OrderController();
    @Override
    public StateEnum handleRequest() {
        try{
            orderController.removeItemFromOrder();
        }catch (Exception e){
            System.err.println("Something went wrong, please try again");
            System.err.println("Message: "+e.getMessage());
        }

        return StateEnum.ORDER_MANAGEMENT;
    }
}
