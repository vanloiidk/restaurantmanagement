package com.company.view;

import com.company.model.SelectOption;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class OptionDashBoardBeauty {
    private ArrayList<SelectOption> options;
    private String title;
    private final int WIDTH = 100;

    public OptionDashBoardBeauty(String title,ArrayList<SelectOption> options) {
        this.options = options;
        this.title = title;
    }

    public void printBeauty(){
        for (int i = 0; i < WIDTH/2; i++) {
            System.out.print("*");
        }
        System.out.print("/**/ "+title+" /**/");
        for (int i = 0; i < WIDTH/2; i++) {
            System.out.print("*");
        }
        System.out.println();

        for (SelectOption option : options) {
            System.out.println((option.getValue()) + ". " + option.getLabel());
        }
        System.out.println();
        System.out.println("Select Option:");

    }

    public static class Builder{
        private ArrayList<SelectOption> options;
        private String title;

        public Builder() {
            this.options = new ArrayList<SelectOption>();
            this.title = "Select options";
        }

        public Builder hasOptions(ArrayList<SelectOption> options){
            this.options = options;
            return this;
        }

        public Builder hasTitle(String title){
            this.title = title;
            return this;
        }

        public OptionDashBoardBeauty build(){
            return new OptionDashBoardBeauty(this.title, this.options);
        }
    }
}
