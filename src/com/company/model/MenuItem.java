package com.company.model;

import java.util.Random;

public class MenuItem {
    private Integer id;
    private String name;
    private String desc;
    private Float price;
    private String image;
    private Integer index;

    public MenuItem(int id, String name, String desc, float price, String image, int index) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.price = price;
        this.image = image;
        this.index = index;
    }

    public MenuItem(String[] columns){
        this.id = Integer.parseInt(columns[0]);
        this.name = columns[1];
        this.desc = columns[2];
        this.price = Float.parseFloat(columns[3]);
        this.image = columns[4];
        this.index = Integer.parseInt(columns[5]);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return id+","+name+","+desc+","+price+","+image+","+index;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj.getClass() != this.getClass()) return false;

        MenuItem menuItem = (MenuItem) obj;
        return this.id == menuItem.getId();
    }

    public String toStringWithBeauty() {
        return "MenuItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", price=" + price +
                ", image='" + image + '\'' +
                ", index=" + index +
                '}';
    }

    public static class MenuItemBuilder{
        private int id;
        private String name;
        private String desc;
        private float price;
        private String image;
        private int index;

        public MenuItemBuilder() {
            Random ran = new Random();
            this.id = ran.nextInt();
            this.name = "";
            this.desc = "";
            this.price = 0;
            this.image = "";
            this.index = 0;
        }

        public MenuItemBuilder hasId(int id){
            this.id = id;
            return this;
        }

        public MenuItemBuilder withName(String name){
            this.name = name;
            return this;
        }

        public  MenuItemBuilder withDesc(String desc){
            this.desc = desc;
            return this;
        }
        public  MenuItemBuilder hasPrice(float price){
            this.price = price;
            return this;
        }
        public  MenuItemBuilder withImage(String image){
            this.image = image;
            return this;
        }
        public  MenuItemBuilder hasIndex(int index){
            this.index = index;
            return this;
        }
        public MenuItem build(){
            validateMenuObject();
            return new MenuItem(
                    this.id,
                    this.name,
                    this.desc,
                    this.price,
                    this.image,
                    this.index
            );
        }
        private void validateMenuObject(){
            if(this.price<=0) throw new IllegalArgumentException("Price can't be negative");
            if(this.index <0) throw new IllegalArgumentException("Index can't be negative");
        }
    }
}
