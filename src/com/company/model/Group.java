package com.company.model;

import com.company.repoIplm.GroupRepoImpl;
import com.company.repository.GroupRepo;

import java.util.Objects;

public class Group {

    private int id;
    private String name;
    private String image;
    private int index;
    private Group parent;
    private static final GroupRepo groupRepo = new GroupRepoImpl();

    public Group(int id, String name, String image, int index, Group parent) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.index = index;
        this.parent = parent;
    }

    public Group(String[] columns){
        this.id = Integer.parseInt(columns[0]);
        this.name = columns[1].equals("null")?null:columns[1];
        this.image = columns[2].equals("null")?null:columns[2];
        this.index = Integer.parseInt(columns[3]);
//        System.out.println(columns[4].getClass());
//        System.out.println(columns[4]);
        if(!columns[4].equals("null")){
//            System.out.println(Long.valueOf(columns[4]));
            this.parent = groupRepo.findByID(Long.valueOf(columns[4]));
        }
        else{
            this.parent = null;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Group getParent() {
        return parent;
    }

    public void setParent(Group parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        String parentId = null;
        if(parent !=null){
            parentId = String.valueOf(parent.getId());
        }
        return id+","+name+","+image+","+index+","+parentId;
    }


    public String toStringWithBeauty() {
        String parentString=null;
        if(parent !=null){
            parentString = parent.toStringWithBeauty();
        }
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", index=" + index +
                ", parent=" + parentString +
                '}';
    }

    public String toStringParentFirst() {
        String parentString = "";
        if(Objects.nonNull(parent)){
            parentString = parent.toStringParentFirst();
        }

        return parentString+"\n"+name+"\n";
    }
}
