package com.company.model;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Human {
    private int id;
    private String name;
    private Address address;
    private int age;

    public Human() {
        this.name = "my name";
        this.age = 20;
        this.id =1;
    }

    public Human(String name, int age, int id) {
        this.name = name;
        this.age = age;
        this.id =id;
    }

    public Human(String[] values){
        this.id = Integer.parseInt(values[0]);
        this.name = values[1];
        this.age = Integer.parseInt(values[2]);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return this.id +","+this.name + "," + this.age;
    }

    private List<String> getKeys(){
        return Arrays.stream(Human.class.getDeclaredFields()).map(Field::getName).collect(Collectors.toList());
    }


    @Override
    public boolean equals(Object obj) {
        if(this==obj) return true;
        if(obj==null || getClass() != obj.getClass()) return false;
        Human newHuman = (Human) obj;
        return id==newHuman.getId() && name.equals(newHuman.getName())&&age== newHuman.getAge();
    }
}
