package com.company.model;

import com.company.helper.DateHelper;
import com.company.repoIplm.OrderItemRepoImpl;
import com.company.repository.OrderItemRepo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Order {
    private Integer id;
    private String customer;
    private Date createdDate;
    private Boolean isPaid;

    private final OrderItemRepo orderItemRepo = new OrderItemRepoImpl();
    public Order(int id, String customer, Date createdDate, Boolean isPaid) {
        this.id = id;
        this.customer = customer;
        this.createdDate = createdDate;
        this.isPaid = isPaid;
    }

    public Order(String[] columns){
        this.id = Integer.parseInt(columns[0]);
        this.customer = columns[1];
        this.createdDate = DateHelper.parse(columns[2]);
        this.isPaid = Boolean.valueOf(columns[3]);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean getPaid() {
        return isPaid;
    }

    public void setPaid(Boolean paid) {
        isPaid = paid;
    }

    public double getTotal(){
        ArrayList<OrderItem> orderItems = orderItemRepo.findByOrderId(id);
        return orderItems.stream().mapToDouble(el -> (el.getMenuItem().getPrice() * el.getQuantity())).sum();
    }

    @Override
    public String toString() {
        return id+","+customer+","+ DateHelper.format(createdDate) +","+isPaid;
    }


    public String toStringWithBeauty() {
        return "Order{" +
                "id=" + id +
                ", customer='" + customer + '\'' +
                ", createdDate=" + DateHelper.format(createdDate) +
                ", isPaid=" + isPaid +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if(obj.getClass() != this.getClass()) return false;

        Order order = (Order) obj;
        return this.id == order.getId();
    }

    public static class OrderBuilder{
        private int id;
        private String customer;
        private Date createdDate;
        private Boolean isPaid;

        public OrderBuilder() {
            this.createdDate = new Date();
            this.isPaid = false;
        }

        public OrderBuilder hasId(int id){
            this.id=id;
            return this;
        }
        public OrderBuilder hasCustomer(String customer){
            this.customer=customer;
            return this;
        }
        public Order build(){
            return new Order(
                    this.id,
                    this.customer,
                    this.createdDate,
                    this.isPaid
            );
        }
    }


}
