package com.company.model;

public class Address {
    private int id;
    private String province;
    private String district;

    public Address(int id, String province, String district) {
        this.id = id;
        this.province = province;
        this.district = district;
    }

    public Address() {
        this.id=1;
        this.province = "Ca Mau";
        this.district = "U Minh";
    }

    public Address(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
