package com.company.model;

import com.company.repoIplm.GroupRepoImpl;
import com.company.repoIplm.MenuItemRepoImpl;
import com.company.repository.GroupRepo;
import com.company.repository.MenuItemRepo;

public class MenuGroup {
    private int id;
    private MenuItem menuItem;
    private Group group;
    private final static MenuItemRepo menuItemRepo = new MenuItemRepoImpl();
    private final static GroupRepo groupRepo = new GroupRepoImpl();

    public MenuGroup(int id, MenuItem menuItem, Group group) {
        this.id = id;
        this.menuItem = menuItem;
        this.group = group;
    }
    public MenuGroup(int id,  int menuItemId, int groupId) {
        this.id = id;
        this.menuItem = menuItemRepo.findByID((long) menuItemId);
        this.group = groupRepo.findByID((long) groupId);
    }

    public MenuGroup(String[] columns){
        this.id = Integer.parseInt(columns[0]);
        this.menuItem = menuItemRepo.findByID(Long.valueOf(columns[1]));
        this.group = groupRepo.findByID(Long.valueOf(columns[2]));
    }

    @Override
    public String toString() {
        return id+","+menuItem.getId()+","+group.getId();
    }

    public String toStringWithBeauty() {
        return "MenuGroup{" +
                "id=" + id +
                ", menuItem=" + menuItem.toStringWithBeauty() +
                ", group=" + group.toStringWithBeauty() +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if(obj.getClass()!=this.getClass()) return false;

        MenuGroup menuGroup = (MenuGroup) obj;
        return this.id == menuGroup.getId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
