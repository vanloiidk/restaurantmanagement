package com.company.model;

import com.company.repoIplm.MenuItemRepoImpl;
import com.company.repoIplm.OrderRepoImpl;
import com.company.repository.MenuItemRepo;
import com.company.repository.OrderRepo;

import java.util.Objects;

public class OrderItem {
    private int id;
    private Order order;
    private MenuItem menuItem;
    private int quantity;
    private final OrderRepo orderRepo = new OrderRepoImpl();
    private final MenuItemRepo menuItemRepo = new MenuItemRepoImpl();

    public OrderItem(int id, Order order, MenuItem menuItem, int quantity) {
        this.id = id;
        this.order = order;
        this.menuItem = menuItem;
        this.quantity = quantity;
    }

    public OrderItem(String[] columns){
        this.id = Integer.parseInt(columns[0]);
        this.order = orderRepo.findByID(Long.valueOf(columns[1]));
        this.menuItem = menuItemRepo.findByID(Long.valueOf(columns[2]));
        this.quantity = Integer.parseInt(columns[3]);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public static class OrderItemBuilder{
        private int id;
        private Order order;
        private MenuItem menuItem;
        private int quantity;

        public OrderItemBuilder(){
        }
        public OrderItemBuilder hasId(int id){
            this.id = id;
            return this;
        }
        public OrderItemBuilder hasOrder(Order order){
            this.order = order;
            return this;
        }
        public OrderItemBuilder hasMenuItem(MenuItem menuItem){
            this.menuItem = menuItem;
            return this;
        }
        public OrderItemBuilder hasQuantity(int quantity){
            this.quantity = quantity;
            return this;
        }

        public OrderItem build(){
            return new OrderItem(
                    this.id,
                    this.order,
                    this.menuItem,
                    this.quantity
            );
        }
    }

    @Override
    public String toString() {
        String orderString = String.valueOf(Objects.nonNull(order)?order.getId():null);
        String menuItemString = String.valueOf(Objects.nonNull(menuItem)?menuItem.getId():null);

        return id+","+orderString+","+menuItemString+","+quantity;
    }

    public String toStringwithBeuty() {
        return "OrderItem{" +
                "id=" + id +
                ", order=" + order.toStringWithBeauty() +
                ", menuItem=" + menuItem.toStringWithBeauty() +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public boolean equals(Object obj) {

        if(obj.getClass()!=this.getClass()) return false;

        OrderItem orderItem = (OrderItem) obj;
        return this.id==orderItem.getId();

    }
}
