package com.company.model;

import com.company.enums.StateEnum;
import com.company.stateMachine.State;

public class SelectOption {
    private int value;
    private String label;
    private StateEnum state;

    public SelectOption(int value, String label, StateEnum state) {
        this.value = value;
        this.label = label;
        this.state = state;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public StateEnum getState() {
        return state;
    }

    public void setState(StateEnum state) {
        this.state = state;
    }
}
