package com.company.repoIplm;

import com.company.helper.FileIOHelper;
import com.company.model.Group;
import com.company.model.Human;
import com.company.model.MenuGroup;
import com.company.model.MenuItem;
import com.company.repository.MenuItemRepo;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Collectors;

public class MenuItemRepoImpl implements MenuItemRepo {


    private final static String FILE_PATH = "./MenuItem.csv";
    private final static FileIOHelper<MenuItem> fileIOHelper = new FileIOHelper<MenuItem>(FILE_PATH,MenuItem.class);

    @Override
    public void save(MenuItem object) {

        fileIOHelper.WriteObjectToFile(object,true);
    }

    @Override
    public void saveAll(ArrayList<MenuItem> array) {
        fileIOHelper.writeToCSV(array,true);
    }

    @Override
    public void delete(MenuItem object) {

    }

    @Override
    public void deleteByID(Long id) {
        MenuItem menuItem = fileIOHelper.findById(id);
        fileIOHelper.deleteRow(menuItem);
    }

    @Override
    public void deleteAll(ArrayList<Human> array) {

    }

    @Override
    public void find(MenuItem object) {

    }

    @Override
    public MenuItem findByID(Long id) {

        return fileIOHelper.findById(id);
    }

    @Override
    public ArrayList<MenuItem> findAll() {

        return fileIOHelper.readAll();
    }

    @Override
    public void updateByID(Long id, MenuItem updated) {
        MenuItem menuItem = fileIOHelper.findById(id);

        if(Objects.isNull(menuItem)) return;

        fileIOHelper.deleteRow(menuItem);
        fileIOHelper.WriteObjectToFile(updated,true);
    }

    @Override
    public int generateID() {
        ArrayList<MenuItem> menuItems = fileIOHelper.readAll();

        if(Objects.isNull(menuItems) || menuItems.size()<=0) return 0;


        menuItems = (ArrayList<MenuItem>) menuItems.stream().sorted(Comparator.comparingInt(MenuItem::getId)).collect(Collectors.toList());
        return menuItems.get(menuItems.size()-1).getId()+1;
    }
}
