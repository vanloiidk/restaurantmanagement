package com.company.repoIplm;

import com.company.helper.FileIOHelper;
import com.company.model.Group;
import com.company.model.Human;
import com.company.model.MenuGroup;
import com.company.model.MenuItem;
import com.company.repository.MenuGroupRepo;
import com.company.repository.MenuItemRepo;

import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Collectors;

public class MenuGroupRepoImpl implements MenuGroupRepo {

    private final static String FILE_PATH = "./MenuGroup.csv";
    private final static FileIOHelper<MenuGroup> fileIOHelper = new FileIOHelper<MenuGroup>(FILE_PATH, MenuGroup.class);

    @Override
    public void save(MenuGroup object) {
        fileIOHelper.WriteObjectToFile(object,true);
    }

    @Override
    public void saveAll(ArrayList<MenuGroup> array) {
        fileIOHelper.writeToCSV(array,true);
    }

    @Override
    public void delete(MenuGroup object) {

    }

    @Override
    public void deleteByID(Long id) {
    }

    @Override
    public void deleteAll(ArrayList<Human> array) {

    }

    @Override
    public void find(MenuGroup object) {

    }

    @Override
    public MenuGroup findByID(Long id) {
        return null;
    }

    @Override
    public ArrayList<MenuGroup> findAll() {
        return fileIOHelper.readAll();
    }

    @Override
    public void updateByID(Long id, MenuGroup updated) {

    }

    @Override
    public int generateID() {
        ArrayList<MenuGroup> menuGroups = fileIOHelper.readAll();

        if(Objects.isNull(menuGroups) || menuGroups.size()<=0) return 0;


        menuGroups = (ArrayList<MenuGroup>) menuGroups.stream().sorted(Comparator.comparingInt(MenuGroup::getId)).collect(Collectors.toList());
        return menuGroups.get(menuGroups.size()-1).getId()+1;
    }

    @Override
    public void deleteByItemId(Long itemID) {
        System.out.println("delete group");
        ArrayList<MenuGroup> menuGroups = fileIOHelper.readAll();
        System.out.println(menuGroups);
        menuGroups = (ArrayList<MenuGroup>) menuGroups.stream().filter(el->el.getMenuItem().getId()==itemID).collect(Collectors.toList());
        System.out.println(menuGroups);
        menuGroups.forEach(fileIOHelper::deleteRow);
    }
}
