package com.company.repoIplm;

import com.company.helper.FileIOHelper;
import com.company.model.Human;
import com.company.model.Order;
import com.company.model.OrderItem;
import com.company.repository.OrderRepo;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Collectors;

public class OrderRepoImpl implements OrderRepo {

    private static final String FILE_PATH="./Order.csv";
    private static final FileIOHelper<Order> fileIOHelper = new FileIOHelper<Order>(FILE_PATH, Order.class);
    @Override
    public void save(Order object) {
        fileIOHelper.WriteObjectToFile(object,true);
    }

    @Override
    public void saveAll(ArrayList<Order> array) {

    }

    @Override
    public void delete(Order object) {
        fileIOHelper.deleteRow(object);
    }

    @Override
    public void deleteByID(Long id) {

    }

    @Override
    public void deleteAll(ArrayList<Human> array) {

    }

    @Override
    public void find(Order object) {

    }

    @Override
    public Order findByID(Long id) {

        return fileIOHelper.findById(id);
    }

    @Override
    public ArrayList<Order> findAll() {
        return null;
    }

    @Override
    public void updateByID(Long id, Order updated) {
        Order order = fileIOHelper.findById(id);
        fileIOHelper.deleteRow(order);
        fileIOHelper.WriteObjectToFile(updated,true);
    }

    @Override
    public int generateID() {
        ArrayList<Order> orders = fileIOHelper.readAll();
        if(Objects.isNull(orders)||orders.size()<=0) return 0;

        orders = (ArrayList<Order>) orders.stream().sorted(Comparator.comparingInt(Order::getId)).collect(Collectors.toList());
        return orders.get(orders.size()-1).getId()+1;
    }
}
