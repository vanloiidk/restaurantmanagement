package com.company.repoIplm;

import com.company.helper.FileIOHelper;
import com.company.model.Group;
import com.company.model.Human;
import com.company.repository.GroupRepo;

import java.util.ArrayList;

public class GroupRepoImpl implements GroupRepo {

    private static final String FILE_PATH = "./Group.csv";
    private static final FileIOHelper<Group> fileIOHelper = new FileIOHelper<Group>(FILE_PATH,Group.class);

    @Override
    public void save(Group object) {

    }

    @Override
    public void saveAll(ArrayList<Group> array) {

        fileIOHelper.writeToCSV(array, true);

    }

    @Override
    public void delete(Group object) {

    }

    @Override
    public void deleteByID(Long id) {

    }

    @Override
    public void deleteAll(ArrayList<Human> array) {

    }

    @Override
    public void find(Group object) {

    }

    @Override
    public Group findByID(Long id) {
        return fileIOHelper.findById(id);
    }

    @Override
    public ArrayList<Group> findAll() {
        return fileIOHelper.readAll();

    }

    @Override
    public void updateByID(Long id, Group updated) {

    }

    @Override
    public int generateID() {
        return 0;
    }
}
