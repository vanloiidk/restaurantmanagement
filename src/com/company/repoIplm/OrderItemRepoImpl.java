package com.company.repoIplm;

import com.company.helper.FileIOHelper;
import com.company.model.Human;
import com.company.model.MenuItem;
import com.company.model.Order;
import com.company.model.OrderItem;
import com.company.repository.OrderItemRepo;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Collectors;

public class OrderItemRepoImpl implements OrderItemRepo {

    private final static String FILE_PATH = "./OrderItem.csv";
    private final static FileIOHelper<OrderItem> fileIOHelper = new FileIOHelper<OrderItem>(FILE_PATH,OrderItem.class);

    @Override
    public void save(OrderItem object) {
        fileIOHelper.WriteObjectToFile(object,true);
    }

    @Override
    public void saveAll(ArrayList<OrderItem> array) {

    }

    @Override
    public void delete(OrderItem object) {
        fileIOHelper.deleteRow(object);
    }

    @Override
    public void deleteByID(Long id) {

    }

    @Override
    public void deleteAll(ArrayList<Human> array) {

    }

    @Override
    public void find(OrderItem object) {

    }

    @Override
    public OrderItem findByID(Long id) {
        ArrayList<OrderItem> orderItems = fileIOHelper.readAll();
        return orderItems.stream().filter(el->el.getId()==id).findFirst().get();
    }

    @Override
    public ArrayList<OrderItem> findAll()
    {
        return fileIOHelper.readAll();
    }

    @Override
    public void updateByID(Long id, OrderItem updated) {

    }

    @Override
    public int generateID()
    {
        ArrayList<OrderItem> orderItems = fileIOHelper.readAll();

        if(Objects.isNull(orderItems) || orderItems.size()<=0) return 0;

        orderItems = (ArrayList<OrderItem>) orderItems.stream().sorted(Comparator.comparingInt(OrderItem::getId)).collect(Collectors.toList());
        return orderItems.get(orderItems.size()-1).getId()+1;
    }

    @Override
    public ArrayList<OrderItem> findByOrderId(long orderID) {
        ArrayList<OrderItem> orderItems = fileIOHelper.readAll();

        return (ArrayList<OrderItem>) orderItems.stream().filter(el->el.getOrder().getId()==orderID).collect(Collectors.toList());

    }

    @Override
    public void deleteByOrderID(long orderID) {
        ArrayList<OrderItem> orderItems = fileIOHelper.readAll();
        orderItems = (ArrayList<OrderItem>) orderItems.stream().filter(el->el.getOrder().getId()==orderID).collect(Collectors.toList());
        orderItems.forEach(fileIOHelper::deleteRow);
    }
}
