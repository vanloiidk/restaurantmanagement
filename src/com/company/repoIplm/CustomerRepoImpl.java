package com.company.repoIplm;

import com.company.helper.FileIOHelper;
import com.company.model.Customer;
import com.company.model.Group;
import com.company.model.Human;
import com.company.repository.CustomerRepo;

import java.util.ArrayList;

public class CustomerRepoImpl implements CustomerRepo {

    private final static String FILE_PATH = "./Customer.csv";
    private final static FileIOHelper<Customer> fileIOHelper = new FileIOHelper<Customer>(FILE_PATH, Customer.class);
    @Override
    public void save(Customer object) {

    }

    @Override
    public void saveAll(ArrayList<Customer> array) {

        fileIOHelper.writeToCSV(array, true);

    }

    @Override
    public void delete(Customer object) {

    }

    @Override
    public void deleteByID(Long id) {

    }

    @Override
    public void deleteAll(ArrayList<Human> array) {

    }

    @Override
    public void find(Customer object) {

    }

    @Override
    public Customer findByID(Long id) {

        return new Customer(1,"test","test");
    }

    @Override
    public ArrayList<Customer> findAll() {

        return null;
    }

    @Override
    public void updateByID(Long id, Customer updated) {

    }

    @Override
    public int generateID() {
        return 0;
    }
}
