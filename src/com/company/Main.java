package com.company;

import com.company.controller.MenuItemController;
import com.company.model.*;
import com.company.repoIplm.GroupRepoImpl;
import com.company.repoIplm.MenuGroupRepoImpl;
import com.company.repository.GroupRepo;
import com.company.repository.MenuGroupRepo;
import com.company.stateMachine.StateMachine;
import com.company.view.OptionDashBoardBeauty;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
//        GroupRepo groupRepo = new GroupRepoImpl();

        /*CREATE GROUPS*/
//        ArrayList<Group> groups = new ArrayList<Group>();
//        Group group = new Group(1,"Food",null,1,null);
//        Group group1 = new Group(2,"Drink",null,4,null);
//        Group group2 = new Group(3,"Lunch",null,2,group);
//        Group group3 = new Group(4,"Dinner",null,3,group);
//        Group group4 = new Group(5,"Alcohol",null,5,group1);
//        groups.add(group);
//        groups.add(group1);
//        groups.add(group2);
//        groups.add(group3);
//        groups.add(group4);
//
//        groupRepo.saveAll(groups);


        /*LOAD GROUPS*/
//        ArrayList<Group> loadedGroups = groupRepo.findAll();

//        System.out.println(loadedGroups);



        /*CREATE CUSTOMER*/
//        ArrayList<Customer> customers = new ArrayList<Customer>();
//
//        Customer customer = new Customer(1, "Nguyen Van Toan","Can Tho");
//        Customer customer2 = new Customer(2, "Tran Quoc Tuan","TP HCM");
//
//        customers.add(customer);
//        customers.add(customer2);
//
//        CustomerRepo customerRepo = new CustomerRepoImpl();
//        customerRepo.saveAll(customers);



        /*CREATE NEW MENU ITEMS*/
//        ArrayList<MenuItem> menuItems = new ArrayList<MenuItem>();
//
//        MenuItem menuItem = new MenuItem(1,"Pizza",30000,"",1);
//        MenuItem menuItem1 = new MenuItem(2,"Rice",20000,"",2);
//        MenuItem menuItem2 = new MenuItem(3,"Fish",10000,"",3);
//        MenuItem menuItem3 = new MenuItem(4,"Chicken",40000,"",4);
//        MenuItem menuItem4 = new MenuItem(5,"Rape juice",50000,"",5);
//        MenuItem menuItem5 = new MenuItem(6,"Rin",20000,"",6);
//        MenuItem menuItem6 = new MenuItem(7,"Strong bow",10000,"",7);
//
//        menuItems.add(menuItem);
//        menuItems.add(menuItem1);
//        menuItems.add(menuItem2);
//        menuItems.add(menuItem3);
//        menuItems.add(menuItem4);
//        menuItems.add(menuItem5);
//        menuItems.add(menuItem6);
//
//        MenuItemRepo menuItemRepo = new MenuItemRepoImpl();
//        menuItemRepo.saveAll(menuItems);


        /*CREATE MENU GROUP*/
//        ArrayList<MenuGroup> menuGroups = new ArrayList<MenuGroup>();
//        MenuGroup menuGroup = new MenuGroup(1,1,4);
//        MenuGroup menuGroup2 = new MenuGroup(2,2,4);
//        MenuGroup menuGroup3 = new MenuGroup(3,2,5);
//        MenuGroup menuGroup4 = new MenuGroup(4,3,5);
//        MenuGroup menuGroup5 = new MenuGroup(5,4,5);
//        MenuGroup menuGroup6 = new MenuGroup(6,5,5);
//        MenuGroup menuGroup7 = new MenuGroup(7,6,5);
//        MenuGroup menuGroup8 = new MenuGroup(8,7,5);
//
//        menuGroups.add(menuGroup);
//        menuGroups.add(menuGroup2);
//        menuGroups.add(menuGroup3);
//        menuGroups.add(menuGroup4);
//        menuGroups.add(menuGroup5);
//        menuGroups.add(menuGroup6);
//        menuGroups.add(menuGroup7);
//        menuGroups.add(menuGroup8);
//
//        MenuGroupRepo menuGroupRepo = new MenuGroupRepoImpl();
//        menuGroupRepo.saveAll(menuGroups);

        /*ADD NEW MENU iTEM*/
//        MenuItemController.createMenuItem();

//        ArrayList<String> options = new ArrayList<>();
//        options.add("test");
//        options.add("test2");
//        OptionDashBoardBeauty optionDashBoardBeauty = new OptionDashBoardBeauty.Builder().hasTitle("Select options").hasOptions(options).build();
//        optionDashBoardBeauty.printBeauty();

        StateMachine.run();
    }
}
