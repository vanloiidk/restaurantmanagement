package com.company.repository;

import com.company.model.MenuGroup;

public interface MenuGroupRepo extends Repository<MenuGroup, Long> {
    public void deleteByItemId(Long itemID);
}
