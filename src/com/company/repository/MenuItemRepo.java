package com.company.repository;

import com.company.model.MenuItem;

public interface MenuItemRepo extends Repository<MenuItem, Long> {
}
