package com.company.repository;

import com.company.model.Order;
import com.company.model.OrderItem;

import java.util.ArrayList;

public interface OrderItemRepo extends Repository<OrderItem, Long>{
    ArrayList<OrderItem> findByOrderId(long orderID);
    void deleteByOrderID(long orderID);
}
