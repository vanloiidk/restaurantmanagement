package com.company.repository;

import com.company.model.Customer;

public interface CustomerRepo extends Repository<Customer, Long> {
}
