package com.company.repository;

import com.company.model.Human;

public interface HumanRepo extends Repository<Human,Long> {
}
