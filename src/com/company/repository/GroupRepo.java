package com.company.repository;

import com.company.model.Group;

public interface GroupRepo extends Repository<Group, Long> {

}
