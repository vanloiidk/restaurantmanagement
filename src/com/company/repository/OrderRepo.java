package com.company.repository;

import com.company.model.Order;

public interface OrderRepo extends Repository<Order, Long>{
}
