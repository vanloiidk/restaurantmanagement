package com.company.repository;

import com.company.model.Group;
import com.company.model.Human;

import java.util.ArrayList;

public interface Repository<K,V> {

    void save(K object);
    void saveAll(ArrayList<K> array);
    void delete(K object);
    void deleteByID(V id);
    void deleteAll(ArrayList<Human> array);
    void find(K object);
    K findByID(V id);
    ArrayList<K> findAll();
    void updateByID(V id, K updated);
    int generateID();
}
