package com.company.controller;

import com.company.model.MenuItem;
import com.company.model.Order;
import com.company.model.OrderItem;
import com.company.repoIplm.MenuItemRepoImpl;
import com.company.repoIplm.OrderItemRepoImpl;
import com.company.repoIplm.OrderRepoImpl;
import com.company.repository.MenuItemRepo;
import com.company.repository.OrderItemRepo;
import com.company.repository.OrderRepo;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public class OrderController {

    private final OrderRepo orderRepo;
    private final OrderItemRepo orderItemRepo;
    private final MenuItemController menuItemController;
    private final MenuItemRepo menuItemRepo;
    public OrderController() {
        this.orderRepo = new OrderRepoImpl();
        this.orderItemRepo = new OrderItemRepoImpl();
        this.menuItemController = new MenuItemController();
        this.menuItemRepo = new MenuItemRepoImpl();
    }

    public void createOrder() throws Exception{
        Scanner sc = new Scanner(System.in);

        Order.OrderBuilder orderBuilder = new Order.OrderBuilder();

        orderBuilder.hasId(orderRepo.generateID());
        System.out.println("Enter customer name:");
        orderBuilder.hasCustomer(sc.nextLine());

        Order order = orderBuilder.build();

        orderRepo.save(order);

        AddItems(order);

        System.out.println("Create order successfully!");
    }

    private void AddItems(Order order){
        Scanner sc = new Scanner(System.in);
        OrderItem.OrderItemBuilder orderItemBuilder = new OrderItem.OrderItemBuilder();
        while(true){
            try{
                menuItemController.ShowMenu();
                System.out.println("Select menu Item (-1 for existing):");
                int menuItemID = sc.nextInt();
                sc.nextLine();

                if(menuItemID==-1) break;

                MenuItem menuItem = menuItemRepo.findByID((long) menuItemID);
                if(Objects.isNull(menuItem)){
                    throw new Exception("Menu item id is invalid");
                }

                System.out.println("Enter quantity:");
                orderItemBuilder.hasQuantity(sc.nextInt());
                sc.nextLine();


                orderItemBuilder.hasId(orderItemRepo.generateID());
                orderItemBuilder.hasOrder(order);
                orderItemBuilder.hasMenuItem(menuItemRepo.findByID((long) menuItemID));


                OrderItem orderItem = orderItemBuilder.build();

                orderItemRepo.save(orderItem);


            }
            catch (InputMismatchException e){
                sc.nextLine();
                System.err.println("Input is invalid");
            }
            catch (Exception e){
                System.err.println(e.getMessage());
            }

        }
    }

    public void showOrder(Boolean isPaid) throws Exception{
        ArrayList<OrderItem> orderItems = orderItemRepo.findAll();

        orderItems = (ArrayList<OrderItem>) orderItems.stream().filter(el-> el.getOrder().getPaid()==isPaid).collect(Collectors.toList());

        Map<Integer, List<OrderItem>> groupped = orderItems.stream().collect(Collectors.groupingBy(el->el.getOrder().getId()));
        printOrderItemWithBeauty(groupped);
    }

    private void printOrderItemWithBeauty(Map<Integer, List<OrderItem>> groupped){

        groupped.forEach((k,v)->{
            Order order = orderRepo.findByID(k.longValue());
            System.out.println(order.toStringWithBeauty());

            for (OrderItem orderItem : v) {
                System.out.println(orderItem.toStringwithBeuty());
            }
            System.out.println("total: "+order.getTotal());

            System.out.println();
        });

    }

    public void payBill() throws Exception{
        Scanner sc = new Scanner(System.in);
        showOrder(false);
        System.out.println("Enter order ID:");
        int orderID = sc.nextInt();
        sc.nextLine();

        Order order = orderRepo.findByID((long) orderID);

        if (Objects.isNull(order)){
            throw new IllegalArgumentException("Order is not found");
        }

        order.setPaid(true);


        orderRepo.updateByID((long) order.getId(),order);

        System.out.println("Pay bill successfully");
    }

    public void deleteBill() throws Exception {
        Scanner sc = new Scanner(System.in);

        showOrder(true);
        System.out.println("Enter bill ID:");
        int orderID = sc.nextInt();
        sc.nextLine();

        Order order = orderRepo.findByID((long) orderID);
        if(Objects.isNull(order)) throw new IllegalArgumentException("Order is not found");

        orderItemRepo.deleteByOrderID(order.getId());
        orderRepo.delete(order);

        System.out.println("Delete bill successfully");
    }

    public void updateOrder() throws Exception {
        Scanner sc = new Scanner(System.in);
        showOrder(false);
        System.out.println("Enter order ID:");
        int orderID = sc.nextInt();
        sc.nextLine();
        Order order = orderRepo.findByID((long) orderID);
        if(Objects.isNull(order)) throw new IllegalArgumentException("Menu Item is not found");


        while (true){
            try{
                System.out.println("Enter update field name(-1 for done): ");
                String fieldName = sc.nextLine();

                if(fieldName.equals("-1")) break;
                if(fieldName.equals("id")) throw new IllegalArgumentException("Can't update id field");

                Field field = order.getClass().getDeclaredField(fieldName);
                field.setAccessible(true);

                System.out.println("Enter new value for ("+field.getName()+"): ");
                String rawValue = sc.nextLine();

                Class type = field.getType();

                field.set(order,type.getDeclaredConstructor(rawValue.getClass()).newInstance(rawValue));

            }catch (Exception e){
                e.printStackTrace();
                System.err.println("Something went wrong, please try again");
            }
        }
        orderRepo.updateByID((long) order.getId(),order);

    }

    public void deleteOrder() throws Exception {
        Scanner sc = new Scanner(System.in);

        showOrder(false);
        System.out.println("Enter order ID:");
        int orderID = sc.nextInt();
        sc.nextLine();

        Order order = orderRepo.findByID((long) orderID);
        if(Objects.isNull(order)) throw new IllegalArgumentException("Order is not found");

        orderItemRepo.deleteByOrderID(order.getId());
        orderRepo.delete(order);

        System.out.println("Delete order successfully");

    }

    public void removeItemFromOrder() throws Exception {
        Scanner sc = new Scanner(System.in);

        showOrder(false);
        System.out.println("Enter Order ID:");
        int orderID = sc.nextInt();
        sc.nextLine();

        Order order = orderRepo.findByID((long) orderID);
        if(Objects.isNull(order)) throw new IllegalArgumentException("Order is not found");

        while (true){
            try{
                showOrderItem(order.getId());
                System.out.println("Enter order item id (-1 for existing):");
                int orderItemID = sc.nextInt();
                sc.nextLine();

                if(orderItemID==-1) break;

                OrderItem orderItem = orderItemRepo.findByID((long) orderItemID);
                if(Objects.isNull(orderItem)) throw new IllegalArgumentException("Order item is not found");

                orderItemRepo.delete(orderItem);
                System.out.println("Remove order item from order successfully");
            }
            catch (Exception e){
                System.err.println("Something went wrong, please try again!!");
                System.err.println("Message: "+e.getMessage());
            }

        }
    }

    public void showOrderItem(int orderID){
        ArrayList<OrderItem> orderItems = orderItemRepo.findByOrderId(orderID);

        orderItems.forEach(el-> System.out.println(el.toStringwithBeuty()));
    }

    public void addItemToOrder() throws Exception {
        Scanner sc = new Scanner(System.in);

        showOrder(false);
        System.out.println("Enter Order ID:");
        int orderID = sc.nextInt();
        sc.nextLine();

        Order order = orderRepo.findByID((long) orderID);
        if(Objects.isNull(order)) throw new IllegalArgumentException("Order is not found");

        while (true){
            try{
                OrderItem.OrderItemBuilder orderItemBuilder = new OrderItem.OrderItemBuilder();
                orderItemBuilder.hasOrder(order);


                menuItemController.ShowMenu();
                System.out.println("Enter item id (-1 for existing):");
                int itemID = sc.nextInt();
                sc.nextLine();

                if(itemID==-1) break;

                MenuItem menuItem = menuItemRepo.findByID((long) itemID);
                if(Objects.isNull(menuItem)) throw new IllegalArgumentException("Menu item is not found");


                orderItemBuilder.hasMenuItem(menuItem);

                System.out.println("Enter quantity");

                orderItemBuilder.hasQuantity(sc.nextInt());
                sc.nextLine();

                OrderItem orderItem = orderItemBuilder.build();


                orderItemRepo.save(orderItem);
                System.out.println("Add item to order successfully");

                showOrderItem(order.getId());

                System.out.println("Do you want to continue (Y/any-keys):");
                String key = sc.nextLine();

                if(!key.equals("Y")&&!key.equals("y"))break;
            }
            catch (Exception e){
                System.err.println("Something went wrong, please try again!!");
                System.err.println("Message: "+e.getMessage());
            }

        }
    }
}
