package com.company.controller;

import com.company.model.Group;
import com.company.model.MenuGroup;
import com.company.model.MenuItem;
import com.company.repoIplm.GroupRepoImpl;
import com.company.repoIplm.MenuGroupRepoImpl;
import com.company.repoIplm.MenuItemRepoImpl;
import com.company.repository.GroupRepo;
import com.company.repository.MenuGroupRepo;
import com.company.repository.MenuItemRepo;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MenuItemController {

    private final MenuItemRepo menuItemRepo;
    private final MenuGroupRepo menuGroupRepo;
    private final GroupRepo groupRepo;

    public MenuItemController() {
        menuItemRepo = new MenuItemRepoImpl();
        menuGroupRepo = new MenuGroupRepoImpl();
        groupRepo = new GroupRepoImpl();
    }

    public void createMenuItem() throws Exception{
        Scanner sc = new Scanner(System.in);
        MenuItem.MenuItemBuilder menuItemBuilder = new MenuItem.MenuItemBuilder();
//        System.out.println("Enter Item ID:");
        menuItemBuilder.hasId(menuItemRepo.generateID());
//        sc.nextLine();
        System.out.println("Enter Item Name:");
        menuItemBuilder.withName(sc.nextLine());
        System.out.println("Enter Item Desc:");
        menuItemBuilder.withDesc(sc.nextLine());
        System.out.println("Enter Item Image:");
        menuItemBuilder.withImage(sc.nextLine());
        System.out.println("Enter Item price:");
        menuItemBuilder.hasPrice(sc.nextFloat());
        sc.nextLine();
        System.out.println("Enter Item index:");
        menuItemBuilder.hasIndex(sc.nextInt());
        sc.nextLine();
        MenuItem menuItem = menuItemBuilder.build();
//        System.out.println(menuItem.toStringWithBeauty());
        menuItemRepo.save(menuItem);

        /*ADD ITEM TO GROUP*/
        this.addToMenuGroup(menuItem);
    }

    public void addToMenuGroup(MenuItem menuItem){
        Scanner sc = new Scanner(System.in);
        while (true){
            try{
                showGroup();
                System.out.println("Enter group ID(-1 for exist)");
                int groupID = sc.nextInt();
                sc.nextLine();

                if(groupID==-1) break;

                Group group = groupRepo.findByID((long) groupID);
                if(Objects.isNull(group)){
                    throw new IllegalArgumentException("Invalid groupID");
                }

                menuGroupRepo.save(new MenuGroup(menuGroupRepo.generateID(),menuItem.getId(),groupID));
                System.out.println("Menu group saved");
            }
            catch (InputMismatchException e){
                sc.nextLine();
                System.err.println("Input is invalid");
            }
            catch (IllegalArgumentException e){
                System.err.println("Group is not found");
            }
            catch (Exception e){
                System.err.println(e);
            }

        }
    }

    public void ShowMenu(){
        ArrayList<MenuItem> menuItems = menuItemRepo.findAll();
        printBeauTyObject(menuItems);
    }
    public void showGroup(){
        ArrayList<Group> groups = groupRepo.findAll();
        printBeautyGroup(groups);
    }

    public void ShowMenuGroup(){
        ArrayList<MenuGroup> menuGroups = menuGroupRepo.findAll();
        Map<Integer, List<MenuGroup>> groupped = menuGroups.stream().collect(Collectors.groupingBy(el->el.getGroup().getId()));
        printBeautyMenuGroup(groupped);
    }

    private void printBeauTyObject(ArrayList<MenuItem> array){
        for (int i = 0; i < array.size(); i++) {
            System.out.println(array.get(i).toStringWithBeauty());
        }

    }
    private void printBeautyGroup(ArrayList<Group> array){
        for (int i = 0; i < array.size(); i++) {
            System.out.println(array.get(i).toStringWithBeauty());
        }
    }

    private void printBeautyMenuGroup(Map<Integer, List<MenuGroup>> groupped){
        groupped.forEach((k,v)->{
            System.out.println(groupRepo.findByID(k.longValue()).toStringParentFirst());
            System.out.println("Items:");
            printArray(v);
        });
    }

    private void printArray(List<MenuGroup> list){
        list.forEach(el->{
            System.out.println(el.getMenuItem().toStringWithBeauty());
        });
    }

    public void deleteById() throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter item ID:");
        int menuItemID = sc.nextInt();
        menuGroupRepo.deleteByItemId((long) menuItemID);
        menuItemRepo.deleteByID((long) menuItemID);

    }

    public void updateItem() throws Exception {

        Scanner sc = new Scanner(System.in);

        ShowMenu();
        System.out.println("Enter item ID:");
        int itemID = sc.nextInt();
        sc.nextLine();
        MenuItem menuItem = menuItemRepo.findByID((long) itemID);
        if(Objects.isNull(menuItem)) throw new IllegalArgumentException("Menu Item is not found");

        while (true){
            try{
                System.out.println("Enter update field name(-1 for done): ");
                String fieldName = sc.nextLine();

                if(fieldName.equals("-1")) break;
                if(fieldName.equals("id")) throw new IllegalArgumentException("Can't update id field");

                Field field = menuItem.getClass().getDeclaredField(fieldName);
                field.setAccessible(true);

                System.out.println("Enter new value for ("+field.getName()+"): ");
                String rawValue = sc.nextLine();

                Class type = field.getType();

                field.set(menuItem,type.getDeclaredConstructor(rawValue.getClass()).newInstance(rawValue));

            }catch (Exception e){
                e.printStackTrace();
                System.err.println("Something went wrong, please try again");
            }
        }
        menuItemRepo.updateByID((long) menuItem.getId(),menuItem);

    }
}
