package com.company.helper;

import com.company.enums.StateEnum;
import com.company.model.SelectOption;

import java.util.ArrayList;

public class OptionArrayListHelper {

    public static StateEnum getStateName(ArrayList<SelectOption> options, int optionValue) throws Exception {
        return options.stream().filter(el->el.getValue()==optionValue).findFirst().orElseThrow(()->new IllegalArgumentException("Invalid selected option")).getState();
    }
}
