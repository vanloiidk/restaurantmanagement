package com.company.helper;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class FileIOHelper<G> {

    private final String filepath;

    private final Class<G> clazz;

    public FileIOHelper(String filepath, Class<G> clazz) {

        this.filepath = filepath;

        this.clazz = clazz;
    }

    public void WriteObjectToFile(G object, Boolean append) {

        try
        {

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.filepath,append), StandardCharsets.UTF_8));
            bw.write(object.toString());
            bw.newLine();
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeToCSV(ArrayList<G> array, Boolean append)
    {
        try
        {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.filepath,append), StandardCharsets.UTF_8));
            for (G object : array)
            {

                bw.write(object.toString());
                bw.newLine();
            }
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void deleteRow(G object){

        ArrayList<G> arr = readAllData();
        ArrayList<G> list = (ArrayList<G>) arr.stream().filter(el->!el.equals(object)).collect(Collectors.toList());
        this.writeToCSV(list,false);
    }

    private G createContents(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        return this.clazz.getDeclaredConstructor(String[].class).newInstance((Object)args);
    }

    public ArrayList<G> readAll(){
        return readAllData();
    }

    public G findById(Long Id){
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(this.filepath), StandardCharsets.UTF_8));

            String line;

            while ((line=br.readLine())!=null){
                String[] columns = line.split(",");
                if(Integer.parseInt(columns[0])  == Id){
                    return createContents(columns);
                }
            }
            br.close();
            return null;

        } catch (IOException | InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<G> readAllData(){
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(this.filepath), StandardCharsets.UTF_8));

            String line;
            ArrayList<G> arr= new ArrayList<G>();

            while ((line=br.readLine())!=null){
                String[] columns = line.split(",");
                G newObj = this.createContents(columns);
                arr.add(newObj);
            }
            br.close();
            return arr;
        }
        catch (NoSuchFileException e){
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
        return null;
    }

    public int generateID() {
        try {
            return readAllData().size()+1;

        }
        catch (NullPointerException e){
            return 0;
        }
        catch (Exception e){
            System.err.println("Something went wrong");;
            return 0;
        }
    }
}
