package com.company.enums;

import com.company.stateMachine.State;

public enum StateEnum {

    START,
    MENU_MANAGEMENT,
    SHOW_MENU,
    BILL_STORAGE_MANAGEMENT,
    ORDER_MANAGEMENT,
    ADD_ITEM,
    REMOVE_ITEM,
    UPDATE_ITEM,
    CREATE_ORDER,
    SHOW_ORDER,
    UPDATE_ORDER,
    DELETE_ORDER,
    REMOVE_ITEM_FROM_ORDER,
    ADD_ITEM_TO_ORDER,
    PAY_BILL,
    SHOW_BILL,
    DELETE_BILL,
    SEARCH_BILL

}